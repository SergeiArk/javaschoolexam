package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(!isStatementValid(statement)||!parenthesesCheck(statement)){
            return null;
        }
        statement = statement.replaceAll("-","+-");     //replacing - with +- to process statements like (-3+5) correctly, cuz if not, i need to split
        String result = calculate(statement);                             //statement by "-" and this causes mistakes
        if(result != null&&result.endsWith(".0")) {
            result = result.substring(0, result.indexOf("."));
        } else if(result != null) {
            result = String.valueOf(Math.round(Double.parseDouble(result)*10000.0)/10000.0);
        }
        return result;
    }

    //Check if there any invalid symbols or symbol sequences in our statement
    private boolean isStatementValid(String statement) {
        if(statement == null || statement.equals("")){
            return false;
        }
        Pattern p = Pattern.compile("[А-Яa-яA-Za-z~`!@#$%^&_<>?,=:;'\"\\\\]+|[+\\-*/.]{2,}|\\((\\/|\\*)|\\d\\(|(\\*|-|/|\\+|-)\\)");
        Matcher m = p.matcher(statement);
        if (m.find()) {
            return false;
        }
        return true;
    }
    //This method calculates all parts of statement
    private String calculate(String statement) {
        while (statement.contains("(")){
            String parenthesesRegExp = "(\\((?:\\[??[^\\(]*?\\)))";     //regExp to find sub-statements inside ()
            Pattern p = Pattern.compile(parenthesesRegExp);
            Matcher m = p.matcher(statement);

            while (m.find()){       //finding all sub-statements which inside (), calculating them, and replacing ()-parts with its result
                int start = m.start(),
                        end = m.end();
                statement = m.replaceFirst(calculate(statement.substring(start+1,end-1)));
                m = p.matcher(statement);
            }
        }

        List<String> digits = new ArrayList<>(Arrays.asList(statement.split("[+*/]"))),         //here im spliting statement 1st time to get list of all digits
                symbols = new ArrayList<>(Arrays.asList(statement.split("-?\\d+\\.?\\d?")));    //and 2nd time to get list of all symbols
        if(!symbols.isEmpty()) symbols.remove(0);
        if(digits.contains("")) digits.remove("");              //so i remove empty results from digits-list, if statement or some of it's sub-statements inside () starts with "-" for example (-3+5)
        while(symbols.contains("*") || symbols.contains("/")){      //calculate all * and / operations
            int i = symbols.indexOf("*"),
                    j = symbols.indexOf("/"), k;
            if(i == -1){
                k = j;
            } else if(j == -1){
                k = i;
            } else k = Math.min(i,j);
            if(symbols.get(k).equals("*")){
                digits.set(k+1,String.valueOf((Double.parseDouble(digits.get(k)) * Double.parseDouble(digits.get(k+1)))));
            }
            else digits.set(k+1,String.valueOf((Double.parseDouble(digits.get(k)) / Double.parseDouble(digits.get(k+1)))));
            digits.remove(k);
            symbols.remove(k);
        }

        while (symbols.contains("/")){
            int i = symbols.indexOf("/");
            digits.set(i+1, String.valueOf((Double.parseDouble(digits.get(i)) / Double.parseDouble(digits.get(i+1)))));
            digits.remove(i);
            symbols.remove(i);
        }

        while (symbols.contains("+")){      //calculate all + and - operations
            int i = symbols.indexOf("+");
            digits.set(i+1, String.valueOf((Double.parseDouble(digits.get(i)) + Double.parseDouble(digits.get(i+1)))));
            digits.remove(i);
            symbols.remove(i);
        }

        while (symbols.contains("-")){
            int i = symbols.indexOf("-");
            digits.set(i+1, String.valueOf((Double.parseDouble(digits.get(i)) - Double.parseDouble(digits.get(i+1)))));
            digits.remove(i);
            symbols.remove(i);
        }

        String result = digits.get(0);
        if(Double.parseDouble(result) == Double.POSITIVE_INFINITY || Double.parseDouble(result) == Double.NEGATIVE_INFINITY){
            result = null;
        }
        return result;
    }

    //Check if there parentheses placed correctly in our statement
    private boolean parenthesesCheck(String statement){
        int counter = 0;
        int openIdx = -1, closeIdx = -1;
        char[] statementArray = statement.toCharArray();

        for(int i = 0; i < statementArray.length; i++){
            if(statementArray[i] == '('){
                if(openIdx != -1) {
                    openIdx = i;
                }
                counter++;
            }
            if(statementArray[i] == ')'){
                if(closeIdx != -1){
                    closeIdx = i;
                }
                counter--;
            }
        }
        if(closeIdx < openIdx || counter != 0) {
            return false;
        }
        return true;
    }
}